package opmode;

import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.teamcode.robot.Robot;
import org.firstinspires.ftc.teamcode.robot.RobotTesting;
import org.firstinspires.ftc.teamcode.subsystems.DifferentialDriveSubsystem;
import org.firstinspires.ftc.teamcode.teleop.TeleOpMode;

@TeleOp(name="Testing TeleOp", group="testing")
public class TestingOpMode extends TeleOpMode {
    DifferentialDriveSubsystem drive;

    @Override
    public void init() {
        super.init();
        Robot robot = new RobotTesting(hardwareMap);
        drive = (DifferentialDriveSubsystem) robot.getMutableSubsystem("drive");
    }

    @Override
    public void loop() {
        super.loop();
        double leftY = gamepad1.leftStick().y;
        drive.drive(Math.pow(gamepad1.leftStick().y, 2) * (leftY < 0 ? -1 : 1),
                gamepad1.rightStick().x);

        telemetry.addLine(String.format("%s", hardwareMap.getAll(Object.class).size()));
        telemetry.update();
    }
}
