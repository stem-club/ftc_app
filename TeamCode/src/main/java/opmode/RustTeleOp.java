package opmode;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.HardwareDevice;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

@TeleOp(name = "Rust TeleOp", group = "testing")
public class RustTeleOp extends LinearOpMode {
    static {
        System.loadLibrary("rust");
    }

    @Override
    public void runOpMode() throws InterruptedException {
        nativeRunOpMode(hardwareMapToMap());
        sleep(100);
    }

    private native void nativeRunOpMode(Map<String, HardwareDevice> hardwareMapContents);

    /**
     * This method is designed to make it easier to work with the HardwareMap class in Rust by
     * turning it into a regular Map that better follows Rust's ownership rules.
     * @return map based on the hardware map that contains each hardware device only once
     */
    private Map<String, HardwareDevice> hardwareMapToMap() {
        Set<HardwareDevice> alreadySeenDevices = new HashSet<>();
        Map<String, HardwareDevice> map = new HashMap<>();

        for (HardwareDevice device : hardwareMap) {
            if (!alreadySeenDevices.contains(device)) {
                alreadySeenDevices.add(device);
                Iterator<String> names = hardwareMap.getNamesOf(device).iterator();
                if (names.hasNext()) {
                    // Only use the first name and ignore the rest
                    map.put(names.next(), device);
                }
            }
        }
        return map;
    }
}
