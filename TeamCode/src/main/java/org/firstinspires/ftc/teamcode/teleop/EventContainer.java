package org.firstinspires.ftc.teamcode.teleop;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

import static org.checkerframework.checker.nullness.NullnessUtil.castNonNull;

/**
 * This class does two things. First, it has a state machine that changes the state of a button based on its last state
 * and whether or not the physical button was pressed on the gamepad. Second, this stores the event handlers for some
 * button and executes them based on its current state.
 * TODO: Split this class based on the two independent tasks that it performs. The event handling portion should
 * probably go into the Button enum.
 */
public class EventContainer {
  private static final Map<ButtonState, Map<Boolean, ButtonState>> mStateMachine = new EnumMap<>(ButtonState.class);

  /*
   * For those who don't know what this block is: the static block runs once before the constructor runs for the first
   * time. In this case, it is used to set up the state machine map. For more information about this block, see:
   * https://stackoverflow.com/questions/335311/static-initializer-in-java
   */
  static {
    Map<Boolean, ButtonState> buttonPressed = new HashMap<>();
    buttonPressed.put(true, ButtonState.HELD);
    buttonPressed.put(false, ButtonState.RELEASED);

    Map<Boolean, ButtonState> buttonReleased = new HashMap<>();
    buttonReleased.put(true, ButtonState.PRESSED);
    buttonReleased.put(false, ButtonState.OFF);

    for (ButtonState state : ButtonState.values()) {
      switch (state) {
        case PRESSED:
        case HELD:
          mStateMachine.put(state, buttonPressed);
          break;
        case RELEASED:
        case OFF:
          mStateMachine.put(state, buttonReleased);
          break;
      }
    }
  }

  private ButtonState mState = ButtonState.OFF;
  private Map<ButtonState, EventListener> mEventHandlers = new HashMap<>();

  /**
   * This method is used to add an event handler to this button.
   *
   * @param event    The state the button must be in for the handler to be called.
   * @param listener The event handler to be called when this button is in the specified state.
   * @return this, to allow for chaining.
   */
  public EventContainer addHandler(ButtonState event, EventListener listener) {
    mEventHandlers.put(event, listener);
    return this;
  }

  /**
   * This method will run the event handler associated with the button's current state.
   *
   * @return this, to allow for chaining.
   */
  public EventContainer handle() {
    EventListener listener = mEventHandlers.get(mState);
    if (listener != null) {
      listener.onEvent();
    }
    return this;
  }

  /**
   * This method sets the current state to the next state based on the current state and the physical button input.
   *
   * @param buttonInput True if the button is being pressed, false if not.
   * @return this, to allow for chaining.
   */
  public EventContainer nextState(boolean buttonInput) {
    mState = castNonNull(castNonNull(mStateMachine.get(mState)).get(buttonInput));
    return this;
  }
}
