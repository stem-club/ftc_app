package org.firstinspires.ftc.teamcode.commands;

import android.support.annotation.Nullable;

import org.firstinspires.ftc.teamcode.errors.CommandUninitializedError;
import org.firstinspires.ftc.teamcode.errors.RequiredSubsystemUnavailableException;
import org.firstinspires.ftc.teamcode.robot.Robot;
import org.firstinspires.ftc.teamcode.subsystems.ImmutableSubsystem;
import org.firstinspires.ftc.teamcode.subsystems.MutableSubsystem;

import java.util.HashSet;
import java.util.Set;

public abstract class Command {
    private @Nullable Robot mRobot;

    public void init(Robot completeRobot) throws RequiredSubsystemUnavailableException {
        Set<String> requiredSubsystems = getRequiredSubsystems();
        Set<String> ifAvailableSubsystems = getIfAvailableSubsystems();

        Set<String> requestedSubsystems =
                new HashSet<>(requiredSubsystems.size() + ifAvailableSubsystems.size());
        requestedSubsystems.addAll(requiredSubsystems);
        requestedSubsystems.addAll(ifAvailableSubsystems);

        Robot robot = completeRobot.subset(requestedSubsystems);

        for (String requiredSubsystem : requiredSubsystems) {
            if (robot.getMutableSubsystem(requiredSubsystem) == null) {
                completeRobot.union(robot);
                throw new RequiredSubsystemUnavailableException(requiredSubsystem);
            }
        }

        mRobot = robot;
    }

    protected abstract void onExecute();

    protected abstract void onFinish();

    public abstract boolean isFinished();

    public final void execute() {
        onExecute();
    }

    public final @Nullable Robot finish() {
        onFinish();
        Robot robot = mRobot;
        mRobot = null;
        return robot;
    }

    protected @Nullable MutableSubsystem getMutableSubsystem(String name) {
        if (mRobot == null) {
            throw new CommandUninitializedError(this);
        }
        return mRobot.getMutableSubsystem(name);
    }

    protected @Nullable ImmutableSubsystem getImmutableSubsystem(String name) {
        if (mRobot == null) {
            throw new CommandUninitializedError(this);
        }
        return mRobot.getImmutableSubsystem(name);
    }

    protected abstract Set<String> getRequiredSubsystems();

    protected abstract Set<String> getIfAvailableSubsystems();
}
