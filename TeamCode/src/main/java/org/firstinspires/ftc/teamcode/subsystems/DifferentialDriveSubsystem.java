package org.firstinspires.ftc.teamcode.subsystems;

import com.qualcomm.robotcore.hardware.DcMotor;

public class DifferentialDriveSubsystem implements MutableSubsystem {
    private DcMotor mLeftMotor;
    private DcMotor mRightMotor;

    public DifferentialDriveSubsystem(DcMotor leftMotor, DcMotor rightMotor) {
        mLeftMotor = leftMotor;
        mRightMotor = rightMotor;
    }

    /**
     * Drive the robot based on a base wheel power and a turning factor.
     */
    public void drive(double power, double turn) {
        double leftPower = power + turn;
        double rightPower = power - turn;
        double max = Math.max(Math.abs(leftPower), Math.abs(rightPower));
        if (max > 1) {
            leftPower /= max;
            rightPower /= max;
        }
        tankDrive(leftPower, rightPower);
    }

    public void tankDrive(double leftPower, double rightPower) {
        mLeftMotor.setPower(leftPower);
        mRightMotor.setPower(rightPower);
    }
}
