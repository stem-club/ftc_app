package org.firstinspires.ftc.teamcode.util;

public class Color {
    public final int r;
    public final int g;
    public final int b;

    public Color(int r, int g, int b) {
        this.r = r;
        this.g = g;
        this.b = b;
    }

    public double distanceTo(Color other) {
        return Math.sqrt(Math.pow(other.r - r, 2) + Math.pow(other.g - g, 2) + Math.pow(other.b - b, 2));
    }
}
