package org.firstinspires.ftc.teamcode.teleop;

import android.support.annotation.CallSuper;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;

public abstract class TeleOpMode extends OpMode {
  protected EventGamepad gamepad1;
  protected EventGamepad gamepad2;

  @Override
  @CallSuper
  public void init() {
    gamepad1 = new EventGamepad(super.gamepad1);
    gamepad2 = new EventGamepad(super.gamepad2);
  }

  /**
   * This method is called in a loop while the robot is running. If this method is overridden, super
   * must be called.
   */
  @Override
  @CallSuper
  public void loop() {
    gamepad1.handleEvents();
    gamepad2.handleEvents();
  }
}
