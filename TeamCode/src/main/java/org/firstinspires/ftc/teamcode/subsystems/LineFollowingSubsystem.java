package org.firstinspires.ftc.teamcode.subsystems;

import org.firstinspires.ftc.teamcode.sensors.ColorSensor;
import org.firstinspires.ftc.teamcode.util.Color;

public class LineFollowingSubsystem implements ImmutableSubsystem {
    private final ColorSensor mColorSensor;
    private final Color mLineColor;
    private final Color mBackgroundColor;
    private final double mColorDist;

    public LineFollowingSubsystem(ColorSensor colorSensor, Color lineColor, Color backgroundColor) {
        mColorSensor = colorSensor;
        mColorSensor.setLedOn(true);
        mLineColor = lineColor;
        mBackgroundColor = backgroundColor;
        mColorDist = mLineColor.distanceTo(mBackgroundColor);
    }

    /**
     * Determines how on the line vs off the line the color sensor currently is.
     * @return Close to 1 if it is off the line, close to 0 if it is on the line, in between if it
     * is on the edge
     */
    public double currentLineToBg() {
        Color currColor = mColorSensor.color();
        return (currColor.distanceTo(mLineColor) - currColor.distanceTo(mBackgroundColor)) / (2 * mColorDist) + 0.5;
    }
}
