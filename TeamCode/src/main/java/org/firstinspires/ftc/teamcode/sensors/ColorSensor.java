package org.firstinspires.ftc.teamcode.sensors;

import org.checkerframework.checker.initialization.qual.UnknownInitialization;
import org.firstinspires.ftc.teamcode.util.Color;

public class ColorSensor implements Sensor {
    private com.qualcomm.robotcore.hardware.ColorSensor mSensor;
    private boolean mLedEnabled;

    public ColorSensor(com.qualcomm.robotcore.hardware.ColorSensor sensor) {
        mSensor = sensor;
        setLedOn(false);
    }

    public void setLedOn(@UnknownInitialization(ColorSensor.class) ColorSensor this, boolean enable) {
        mLedEnabled = enable;
        mSensor.enableLed(enable);
    }

    public boolean getLedOn() {
        return mLedEnabled;
    }

    public int lightLevel() {
        return mSensor.alpha();
    }

    public Color color() {
        return new Color(mSensor.red(), mSensor.green(), mSensor.blue());
    }

    @Override
    public boolean calibrate() {
        return false;
    }

    @Override
    public String saveCalibration() {
        return "";
    }

    @Override
    public boolean loadCalibration(String calibration) {
        return false;
    }
}
