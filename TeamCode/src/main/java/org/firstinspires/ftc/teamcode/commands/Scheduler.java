package org.firstinspires.ftc.teamcode.commands;

import org.firstinspires.ftc.teamcode.errors.RequiredSubsystemUnavailableException;
import org.firstinspires.ftc.teamcode.robot.Robot;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Scheduler {
    private final List<Command> mRunningCommands;
    private final Robot mRobot;

    public Scheduler(Robot robot) {
        mRunningCommands = new ArrayList<>();
        mRobot = robot;
    }

    public boolean scheduleCommand(Command command) {
        try {
            command.init(mRobot);
        } catch (RequiredSubsystemUnavailableException e) {
            return false;
        }
        mRunningCommands.add(command);
        return true;
    }

    public void run() {
        Iterator<Command> commandIterator = mRunningCommands.iterator();
        while (commandIterator.hasNext()) {
            Command command = commandIterator.next();
            if (command.isFinished()) {
                Robot result = command.finish();
                if (result != null) {
                    mRobot.union(result);
                }
                commandIterator.remove();
            } else {
                command.execute();
            }
        }
    }
}
