package org.firstinspires.ftc.teamcode.teleop;

import java.util.EnumMap;
import java.util.Map;

import static org.checkerframework.checker.nullness.NullnessUtil.castNonNull;

/*
 * Quick note to anyone maintaining this class and the associated event handling classes: I had a
 * code review done and got good advice. I would recommend taking a look at it before refactoring.
 * https://codereview.stackexchange.com/questions/203642/event-handling-gamepad-in-java
 */

/**
 * This class is designed to add event handling to gamepads. For example, if the robot should move a lift up when the
 * up button is pressed, an event can be attached so that the lift will move up when the up button is pressed.
 */
public class EventGamepad {

  private final Map<Button, EventContainer> mButtonEvents = new EnumMap<>(Button.class);
  private final com.qualcomm.robotcore.hardware.Gamepad mGamepad;

  /**
   * Create a new instance of this class based on one of the gamepads provided by the FTC SDK. It should be updated as
   * the physical gamepad is updated.
   *
   * @param gamepad The gamepad to use to determine if buttons are pressed and get joystick values.
   */
  public EventGamepad(com.qualcomm.robotcore.hardware.Gamepad gamepad) {
    mGamepad = gamepad;

    // Fill mButtonEvents so that it is guaranteed that every button will have a valid value.
    for (Button button : Button.values()) {
      mButtonEvents.put(button, new EventContainer());
    }
  }

  /**
   * This method should be called during initialisation and is used to specify what should happen and under what
   * conditions. It can be called multiple times with different buttons and states. If this method is called twice on
   * the same button with the same event, the old event handler will be overwritten by the newer one.
   *
   * @param event    The event that should trigger the event listener.
   * @param button   The button that the event should occur on.
   * @param listener The event listener containing the code to run under the correct conditions.
   */
  public void on(ButtonState event, Button button, EventListener listener) {
    EventContainer eventContainer = castNonNull(mButtonEvents.get(button));
    eventContainer.addHandler(event, listener);
  }

  /**
   * This method should be called once every iteration. On every call, it will run the event handlers associated with
   * the current events.
   */
  public void handleEvents() {
    for (Map.Entry<Button, EventContainer> entry : mButtonEvents.entrySet()) {
      entry.getValue().nextState(entry.getKey().extract(mGamepad)).handle();
    }
  }

  /**
   * Get the position of the left stick.
   *
   * @return The position of the left stick.
   */
  public StickPosition leftStick() {
    return new StickPosition(mGamepad.left_stick_x, mGamepad.left_stick_y);
  }

  /**
   * Get the position of the right stick.
   *
   * @return The position of the right stick.
   */
  public StickPosition rightStick() {
    return new StickPosition(mGamepad.right_stick_x, mGamepad.right_stick_y);
  }

  /**
   * Get the amount that the left trigger is pressed.
   *
   * @return The amount that the left trigger is pressed. 0 means that the trigger is not pressed, 1 means that it is
   * fully depressed.
   * TODO: Make sure that the range really is 0 to 1
   */
  public float leftTrigger() {
    return mGamepad.left_trigger;
  }

  /**
   * Get the amount that the right trigger is pressed.
   *
   * @return The amount that the right trigger is pressed. 0 means that the trigger is not pressed, 1 means that it is
   * fully depressed.
   * TODO: Make sure that the range really is 0 to 1
   */
  public float rightTrigger() {
    return mGamepad.right_trigger;
  }
}
