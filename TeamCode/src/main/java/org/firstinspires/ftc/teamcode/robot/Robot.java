package org.firstinspires.ftc.teamcode.robot;

import android.support.annotation.Nullable;

import org.firstinspires.ftc.teamcode.subsystems.ImmutableSubsystem;
import org.firstinspires.ftc.teamcode.subsystems.MutableSubsystem;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * This class is designed to hold subsystems for Commands to use. A command should request a subset
 * of the mutable subsystems, and will then be given a Robot containing those mutable subsystems and
 * all immutable subsystems. A Robot containing the remaining mutable subsystems and all immutable
 * subsystems will be kept for other Commands to take a subset of.
 *
 * <h1>Mutable subsystem vs immutable subsystem</h1>
 * <h2>Mutable</h2>
 * <ul>
 *     <li>Only one Command can access at a time</li>
 *     <li>Has outputs (eg. motors)</li>
 *     <li>Can have inputs (eg. color sensor)</li>
 *     <li>Defining factor: internal state <em>can</em> be changed by methods</li>
 * </ul>
 * <h2>Immutable</h2>
 * <ul>
 *     <li>Any number of Commands can access at once</li>
 *     <li>No outputs (eg. motors), only has inputs (eg. color sensor)</li>
 *     <li>Defining factor: internal state <em>cannot</em> be changed by methods</li>
 *     <li>Java does not enforce mutability, so manually check that they are actually immutable</li>
 * </ul>
 *
 * <h1>Note</h1>
 * <p>
 *     This was originally written in Rust, then ported to Java. This version is potentially unsafe,
 *     depending on the way it is used. In the future, the NDK should be used to put the guaranteed
 *     to be safe Rust version into the app instead of this Java version.
 *     TODO: Use the NDK/JNI to implement this functionality in Rust
 *     TODO: Alternatively, look into locks for extra safety
 * </p>
 */
public class Robot {
    private Map<String, ImmutableSubsystem> mImmutableSubsystems;
    private Map<String, MutableSubsystem> mMutableSubsystems;

    public Robot(Map<String, ImmutableSubsystem> immutableSubsystems,
                 Map<String, MutableSubsystem> mutableSubsystems) {
        mImmutableSubsystems = immutableSubsystems;
        mMutableSubsystems = mutableSubsystems;
    }

    public Robot() {
        mImmutableSubsystems = new HashMap<>();
        mMutableSubsystems = new HashMap<>();
    }

    /**
     * Split off part of this robot. This method will return a new robot with all the immutable
     * subsystems in this robot and the requested mutable subsystems. The requested mutable
     * subsystems will be removed from this robot. If the requested subroutines are not in this
     * robot, they are ignored.
     * @param mutableSubsystems Set of the names of requested mutable subsystems
     * @return New robot with all immutable subsystems and available requested mutable subsystems
     */
    public Robot subset(Set<String> mutableSubsystems) {
        Map<String, MutableSubsystem> mutable = new HashMap<>();

        Iterator<Map.Entry<String, MutableSubsystem>> it = mMutableSubsystems.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, MutableSubsystem> entry = it.next();
            String key = entry.getKey();

            if (mutableSubsystems.contains(key)) {
                mutable.put(key, entry.getValue());
                it.remove();
            }
        }

        return new Robot(mImmutableSubsystems, mutable);
    }

    /**
     * Move subsystems to this robot from the other robot. It is the opposite operation from subset.
     * After calling, the reference to the otherRobot should be dropped.
     * @param otherRobot The robot containing the subsystems to move
     */
    public void union(Robot otherRobot) {
        for (Map.Entry<String, MutableSubsystem> entry : otherRobot.mMutableSubsystems.entrySet()) {
            mMutableSubsystems.put(entry.getKey(), entry.getValue());
        }
        otherRobot.mMutableSubsystems.clear();
    }

    /**
     * Get an immutable subsystem. Its reference should not be stored longer than the reference to
     * the robot it was retrieved from.
     * @param name Name of the subsystem
     * @return Immutable subsystem with the given name or null if it does not exist
     */
    public @Nullable ImmutableSubsystem getImmutableSubsystem(String name) {
        return mImmutableSubsystems.get(name);
    }

    /**
     * Set an immutable subsystem. The reference to the subsystem should not be stored after being
     * set. The only copy of that reference should exist only in this class and should only be
     * retrieved with {@link #getImmutableSubsystem(String)}.
     * @param name The name of the immutable subsystem to add. If there is already an immutable
     *             subsystem with that name, it will be overwritten.
     * @param subsystem The immutable subsystem
     */
    public void setImmutableSubsystem(String name, ImmutableSubsystem subsystem) {
        mImmutableSubsystems.put(name, subsystem);
    }

    /**
     * Get a mutable subsystem. Its reference should not be stored longer than the reference to the
     * robot it was retrieved from. This is incredibly important for mutable subsystems because
     * they don't add safety if there are several references to them stored in client code.
     * @param name Name of the subsystem
     * @return Mutable subsystem with the given name of null if it does not exist
     */
    public @Nullable MutableSubsystem getMutableSubsystem(String name) {
        return mMutableSubsystems.get(name);
    }

    /**
     * Set a mutable subsystem. The reference to the subsystem should not be stored after being set.
     * The only copy of that reference should exist only in this class and should only be retrieved
     * with {@link #getMutableSubsystem(String)}.
     * @param name The name of the mutable subsystem to add. If there is already a mutable subsystem
     *             with that name, it will be overwritten.
     * @param subsystem The imutable subsystem
     */
    public void setMutableSubsystem(String name, MutableSubsystem subsystem) {
        mMutableSubsystems.put(name, subsystem);
    }

    @Override
    public String toString() {
        return "Immutable: " + mImmutableSubsystems + ", Mutable: " + mMutableSubsystems;
    }
}
