package org.firstinspires.ftc.teamcode.errors;

public class RequiredSubsystemUnavailableException extends Exception {
    private String mSubsystemName;

    public RequiredSubsystemUnavailableException(String subsystemName) {
        mSubsystemName = subsystemName;
    }

    @Override
    public String toString() {
        return "Subsystem unavailable or does not exist: " + mSubsystemName;
    }
}
