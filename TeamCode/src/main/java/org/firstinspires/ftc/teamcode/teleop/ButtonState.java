package org.firstinspires.ftc.teamcode.teleop;

public enum ButtonState {
  PRESSED, // Button was just pressed
  HELD, // The button has been pressed for more than one iteration
  RELEASED, // The button was just released
  OFF // The button has been released for more than one iteration
}