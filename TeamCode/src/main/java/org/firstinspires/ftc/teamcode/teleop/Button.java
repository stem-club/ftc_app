package org.firstinspires.ftc.teamcode.teleop;

import com.qualcomm.robotcore.hardware.Gamepad;

public enum Button {
  A {
    @Override
    public boolean extract(com.qualcomm.robotcore.hardware.Gamepad gamepad) {
      return gamepad.a;
    }
  },
  B {
    @Override
    public boolean extract(com.qualcomm.robotcore.hardware.Gamepad gamepad) {
      return gamepad.b;
    }
  },
  X {
    @Override
    public boolean extract(com.qualcomm.robotcore.hardware.Gamepad gamepad) {
      return gamepad.x;
    }
  },
  Y {
    @Override
    public boolean extract(com.qualcomm.robotcore.hardware.Gamepad gamepad) {
      return gamepad.y;
    }
  },
  UP {
    @Override
    public boolean extract(Gamepad gamepad) {
      return gamepad.dpad_up;
    }
  },
  DOWN {
    @Override
    public boolean extract(Gamepad gamepad) {
      return gamepad.dpad_down;
    }
  },
  LEFT {
    @Override
    public boolean extract(Gamepad gamepad) {
      return gamepad.dpad_left;
    }
  },
  RIGHT {
    @Override
    public boolean extract(Gamepad gamepad) {
      return gamepad.dpad_right;
    }
  },
  LEFT_BUMPER {
    @Override
    public boolean extract(Gamepad gamepad) {
      return gamepad.left_bumper;
    }
  },
  RIGHT_BUMPER {
    @Override
    public boolean extract(Gamepad gamepad) {
      return gamepad.right_bumper;
    }
  },
  LEFT_STICK {
    @Override
    public boolean extract(Gamepad gamepad) {
      return gamepad.left_stick_button;
    }
  },
  RIGHT_STICK {
    @Override
    public boolean extract(Gamepad gamepad) {
      return gamepad.right_stick_button;
    }
  };

  public abstract boolean extract(com.qualcomm.robotcore.hardware.Gamepad gamepad);
}