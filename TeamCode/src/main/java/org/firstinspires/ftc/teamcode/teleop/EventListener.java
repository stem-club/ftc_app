package org.firstinspires.ftc.teamcode.teleop;

public interface EventListener {
  void onEvent();
}