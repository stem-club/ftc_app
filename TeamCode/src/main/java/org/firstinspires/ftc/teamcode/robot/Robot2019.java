package org.firstinspires.ftc.teamcode.robot;

import org.firstinspires.ftc.teamcode.subsystems.ExampleImmutableSubsystem;
import org.firstinspires.ftc.teamcode.subsystems.ImmutableSubsystem;
import org.firstinspires.ftc.teamcode.subsystems.MutableSubsystem;

import java.util.HashMap;
import java.util.Map;

public class Robot2019 extends Robot {
    private static final Map<String, ImmutableSubsystem> immutableSubsystems;

    static {
        immutableSubsystems = new HashMap<>();
        immutableSubsystems.put("example", new ExampleImmutableSubsystem());
    }

    public Robot2019(Map<String, MutableSubsystem> mutableSubsystems) {
        super(immutableSubsystems, mutableSubsystems);
    }
}
