package org.firstinspires.ftc.teamcode.errors;

import org.firstinspires.ftc.teamcode.commands.Command;

public class CommandUninitializedError extends Error {
    private final String mCommandName;

    public CommandUninitializedError(Command command) {
        mCommandName = command.getClass().getSimpleName();
    }

    @Override
    public String toString() {
        return String.format("Command %snot initialized but accessed",
                mCommandName.equals("") ? "": mCommandName + ' ');
    }
}
