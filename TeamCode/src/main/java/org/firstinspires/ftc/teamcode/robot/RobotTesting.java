package org.firstinspires.ftc.teamcode.robot;

import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.HardwareMap;

import org.firstinspires.ftc.teamcode.subsystems.DifferentialDriveSubsystem;
import org.firstinspires.ftc.teamcode.subsystems.ExampleImmutableSubsystem;
import org.firstinspires.ftc.teamcode.subsystems.ImmutableSubsystem;
import org.firstinspires.ftc.teamcode.subsystems.MutableSubsystem;

import java.util.HashMap;
import java.util.Map;

public class RobotTesting extends Robot {
    public RobotTesting(HardwareMap hardwareMap) {
        DcMotor leftMotor = hardwareMap.get(DcMotor.class, "left_motor");
        DcMotor rightMotor = hardwareMap.get(DcMotor.class, "right_motor");
        rightMotor.setDirection(DcMotorSimple.Direction.REVERSE);
        setMutableSubsystem("drive", new DifferentialDriveSubsystem(leftMotor, rightMotor));
    }
}
