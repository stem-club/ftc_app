package org.firstinspires.ftc.teamcode.teleop;

public class StickPosition {
  public final float x;
  public final float y;

  public StickPosition(float x, float y) {
    this.x = x;
    this.y = y;
  }
}
