use jni::objects::JString;
use jni::JNIEnv;
use log::{error, info};

// TODO: Allocate space in the HashSet based on number of arguments
#[macro_export]
macro_rules! hash_set {
    ( $( $x:expr ),* ) => {
        {
            let mut temp_set = std::collections::HashSet::new();
            $(
                temp_set.insert($x);
            )*
            temp_set
        }
    };
}

pub fn str_to_jstring<'a>(rust_str: &'a str, env: &'a JNIEnv<'a>) -> JString<'a> {
    match env.new_string(rust_str) {
        Ok(jstring) => {
            info!("Successful string conversion");
            jstring
        }
        Err(err) => {
            error!("Error creating JString: {}", err);
            panic!("Error creating JString: {}", err)
        }
    }
}
