#![cfg(target_os = "android")]

use android_logger::Config;
use jni::objects::JObject;
use jni::JNIEnv;
use log::{error, Level};

use crate::err::{RegisterHardwareError, RobotError};
use crate::hardware::dcmotor::{DcMotorFactory, Direction};
use crate::hardware::hwmap::{HWMap, HWMapFactory};
use crate::linear_opmode::LinearOpMode;
use crate::robot_lib::robot::Robot;
use crate::robot_lib::system::drivetrain::Drivetrain;
use crate::robot_lib::system::System;

pub mod err;
pub mod hardware;
pub mod linear_opmode;
pub mod robot_lib;
pub mod util;

/// Initialize the `HWMap` that is used in the `LinearOpMode`. Update this method to reflect changes
/// in the robot's configuration.
///
/// # Parameters
///  - `hardware_map_contents`: Java `Map<String, HardwareDevice>` where exactly one name
/// corresponds to one `HardwareDevice` and there is only one name per `HardwareDevice`. This helps
/// ensure that Rust's guarantees are upheld.
///  - `env`: The JNI environment used to interface with Java
fn create_hwmap<'a>(
    hardware_map_contents: JObject<'a>,
    env: &'a JNIEnv<'a>,
) -> Result<HWMap<'a>, RegisterHardwareError> {
    Ok(HWMapFactory::start(hardware_map_contents, env)
        .register_hardware("left_motor", DcMotorFactory::create_hardware)?
        .register_hardware("right_motor", DcMotorFactory::create_hardware)?
        .build())
}

/// Initialize the `Robot` that should be used. Update this method to reflect changes in the robot's
/// configuration.
///
/// # Parameters
///  - `hwmap`: The `HWMap` the robot should get its hardware from
fn create_robot<'a>(hwmap: &mut HWMap<'a>) -> Result<Robot<'a>, RobotError> {
    let left_motor = hwmap.take("left_motor")?.dc_motor()?.build();

    let right_motor = hwmap
        .take("right_motor")?
        .dc_motor()?
        .direction(Direction::Reverse)
        .build();

    Ok(Robot::start()
        .mutable(
            "drivetrain",
            System::Drivetrain(Drivetrain::new(left_motor, right_motor)),
        )
        .build())
}

/// Contains the code that should actually run. Analogous to Java's `runOpMode`.
///
/// # Parameters
///  - `opmode`: The `LinearOpmode` that would be accessed through `this` in Java.
///
/// # Panics
/// This function may panic if there is an error communication with Java. A panic is most likely to
/// happen if the `OpMode`'s `HWMap` is misconfigured.
fn run_opmode<'a>(
    hardware_map_contents: JObject<'a>,
    this: JObject<'static>,
    env: &JNIEnv<'a>,
) -> Result<(), RobotError> {
    android_logger::init_once(
        Config::default()
            .with_min_level(Level::Trace)
            .with_tag("FTCRobotControllerRust"),
    );

    let mut opmode = LinearOpMode::new(&env, this, create_hwmap(hardware_map_contents, &env)?);
    let mut robot_owned = create_robot(opmode.hw_map())?;
    let robot = robot_owned.subset();

    opmode.wait_for_start();

    let (mut drivable_robot, robot) = robot.subset(&hash_set!["drivetrain"]);

    let drivetrain = drivable_robot
        .mutable_system("drivetrain")
        .unwrap()
        .drivetrain()
        .unwrap();

    drivetrain.drive(1.0, 0.0);

    opmode.sleep(60000);

    Ok(())
}

#[allow(non_snake_case)]
pub mod android {
    use super::*;

    #[no_mangle]
    /// This function is actually called by Java and is the entry point to the program.
    ///
    /// # Parameters
    ///  - `env`: The JNI environment used to interface with Java
    ///  - `this`: The Java object that would be represented by this in the context this function
    /// was called. It should be a `LinearOpMode`.
    ///  - `hardware_map_contents`: Java `Map<String, HardwareDevice>` where exactly one name
    /// corresponds to one `HardwareDevice` and there is only one name per `HardwareDevice`. This
    /// helps ensure that Rust's guarantees are upheld.
    pub unsafe extern "C" fn Java_opmode_RustTeleOp_nativeRunOpMode<'a>(
        env: JNIEnv<'a>,
        this: JObject<'static>,
        hardware_map_contents: JObject<'static>,
    ) {
        if let Err(err) = run_opmode(hardware_map_contents, this, &env) {
            error!("Error running OpMode: {}", err);
        };
    }
}
