use std::error::Error;
use std::fmt;
use std::fmt::{Display, Formatter};

#[derive(Debug)]
/// Represents any error that could occur within the Rust code.
pub enum RobotError {
    /// Occurs when Rust expects hardware to exist, but there is no hardware attached to the name
    /// given. Eg. a user tried to take "left_motor", but there is no hardware with that name.
    /// Tuple: (attempted name)
    UndefinedHardware(String),
    /// Occurs when Rust expects hardware to be one type, but it is actually another type of
    /// hardware. Eg. a user tried to convert "color_sensor" to a DC Motor, but it is actually a
    /// color sensor.
    /// Tuple: (expected hardware type, actual hardware type)
    HardwareType(String, String),
    /// Occurs when Rust expects a `System` to exist, but there is no `System` with the name given.
    /// Eg. a user tried to get a `System` called "drivetrain", but it does not exist.
    /// Tuple: (attempted name)
    UndefinedSystem(String),
    /// Occurs when Rust expects a system to be one type, but it is actually another type. Eg. a
    /// user tried to convert "lift" to a drivetrain, but it is actually a lift.
    /// Tuple: (expected system type, actual system type)
    SystemType(String, String),
    /// Represents any `RegisterHardwareError` except `JNIError`, which is converted into
    /// `RobotError::JNIError`.
    RegisterHardwareError(RegisterHardwareError),
    /// Occurs when the JNI has an unexpected error, eg. a method or field cannot be found.
    JNIError(jni::errors::Error),
}

impl Display for RobotError {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        match self {
            RobotError::UndefinedHardware(device_name) => {
                write!(f, "Hardware \"{}\" not defined in HW map", device_name)
            }
            RobotError::HardwareType(expected, actual) => write!(
                f,
                "Hardware converted to \"{}\", should be \"{}\"",
                expected, actual
            ),
            RobotError::UndefinedSystem(system_name) => {
                write!(f, "System \"{}\" not defined", system_name)
            }
            RobotError::SystemType(expected, actual) => write!(
                f,
                "System converted to \"{}\", should be \"{}\"",
                expected, actual
            ),
            RobotError::RegisterHardwareError(error) => write!(f, "{}", error.to_string()),
            RobotError::JNIError(error) => write!(f, "JNI error, type is \"{}\"", error.0),
        }
    }
}

impl Error for RobotError {
    fn cause(&self) -> Option<&Error> {
        match self {
            RobotError::UndefinedHardware(_)
            | RobotError::HardwareType(_, _)
            | RobotError::UndefinedSystem(_)
            | RobotError::SystemType(_, _) => None,
            RobotError::RegisterHardwareError(error) => Some(error),
            RobotError::JNIError(error) => Some(error),
        }
    }
}

impl From<RegisterHardwareError> for RobotError {
    fn from(error: RegisterHardwareError) -> Self {
        if let RegisterHardwareError::JNIError(cause) = error {
            RobotError::JNIError(cause)
        } else {
            RobotError::RegisterHardwareError(error)
        }
    }
}

#[derive(Debug)]
/// Represents an error that can occur while registering hardware
pub enum RegisterHardwareError {
    /// Occurs when the user registers hardware that does not exist in the Java `HardwareMap`. Eg. a
    /// user tries to register hardware called "left_motor", but it does not exist.
    DoesNotExist(String, jni::errors::Error),
    /// Occurs when Java throws some exception.
    UnknownException(jni::errors::Error),
    /// Occurs when the JNI has an unexpected error, eg. a method or field cannot be found.
    JNIError(jni::errors::Error),
}

impl Display for RegisterHardwareError {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        match self {
            RegisterHardwareError::DoesNotExist(name, _) => write!(
                f,
                "Hardware with name \"{}\" does not exist in Java HardwareMap",
                name
            ),
            RegisterHardwareError::UnknownException(_) => write!(f, "Unknown Java exception"),
            RegisterHardwareError::JNIError(error) => write!(f, "JNI error, type: {}", error.0),
        }
    }
}

impl Error for RegisterHardwareError {
    fn cause(&self) -> Option<&Error> {
        match self {
            RegisterHardwareError::DoesNotExist(_, cause)
            | RegisterHardwareError::UnknownException(cause)
            | RegisterHardwareError::JNIError(cause) => Some(cause),
        }
    }
}
