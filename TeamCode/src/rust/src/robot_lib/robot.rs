use std::collections::{HashMap, HashSet};

use crate::robot_lib::system::{BorrowedMutSystems, BorrowedSystems, System, Systems};

#[derive(Debug)]
pub struct Factory<'a> {
    immutable_systems: Systems<'a>,
    mutable_systems: Systems<'a>,
}

impl<'a> Factory<'a> {
    pub fn immutable(mut self, name: &'static str, system: System<'a>) -> Self {
        self.immutable_systems.insert(name, system);
        self
    }

    pub fn mutable(mut self, name: &'static str, system: System<'a>) -> Self {
        self.mutable_systems.insert(name, system);
        self
    }

    pub fn build(self) -> Robot<'a> {
        Robot {
            immutable_systems: self.immutable_systems,
            mutable_systems: self.mutable_systems,
        }
    }
}

#[derive(Debug)]
/// High level struct that owns mutable and immutable systems. A `Robot` is used by taking a subset.
/// This splits the robot into two `RobotSubset`s. One will have the mutable systems requested for
/// the subset, and the other will have the mutable systems not requested for the subset. This means
/// that all mutable systems will only have a single copy and none will be lost. Both subsets will
/// have all immutable systems.
///
/// # What are systems?
/// `System`s are a high level concept that represent large scale pieces of robot hardware. For
/// example, a drivetrain would be represented by a `System`. `System`s usually hold `Hardware` that
/// they manipulate. For example, the drivetrain would have DC motors that are used for driving.
///
/// # What are mutable/immutable systems?
/// A mutable `System` has an internal state that can be modified. For example, a drivetrain would
/// change the internal state of the DC motors that it owns, which makes it mutable. Immutable
/// systems do not have internal state that can change. For example, a sensor cannot be modified, so
/// a system made up of only sensors would be immutable. Mutability is enforced through Rust's
/// mutability rules.
pub struct Robot<'a> {
    immutable_systems: Systems<'a>,
    mutable_systems: Systems<'a>,
}

impl<'a> Robot<'a> {
    /// Start creating a `Robot` using a `Factory`.
    pub fn start() -> Factory<'a> {
        Factory {
            immutable_systems: HashMap::new(),
            mutable_systems: HashMap::new(),
        }
    }

    /// Get the borrowed subset from the `Robot`.
    pub fn subset(&'a mut self) -> Subset<'a> {
        let mut mutable = HashMap::new();
        for (system_name, system) in &mut self.mutable_systems {
            mutable.insert(*system_name, system);
        }

        Subset {
            immutable_systems: &self.immutable_systems,
            mutable_systems: mutable,
        }
    }
}

#[derive(Debug)]
pub struct Subset<'a> {
    immutable_systems: BorrowedSystems<'a>,
    mutable_systems: BorrowedMutSystems<'a>,
}

impl<'a> Subset<'a> {
    /// Take a subset of the subset.
    ///
    /// # Parameters
    ///  - `mutable_systems`: A set of the names of the mutable `System`s that should be in the left
    /// subset. If mutable `Systems` that don't exist are requested, there will be no error or
    /// panic. It will just be missing from the left subset. A `hash_set!` macro is provided in this
    /// crate (util module) for your convenience. It is used the same way that `vec!` is used.
    /// TODO: Return an error if requested `System`s don't exist.
    ///
    /// # Return
    /// This returns two `Subset`s in a tuple. The left `Subset` contains the requested mutable
    /// `System`s and the right `Subset` contains the remaining `System`s.
    pub fn subset(self, mutable_systems: &HashSet<&'static str>) -> (Subset<'a>, Subset<'a>) {
        let mut left_mutable = HashMap::new();
        let mut right_mutable = HashMap::new();

        for (system_name, system) in self.mutable_systems {
            if mutable_systems.contains(system_name) {
                left_mutable.insert(system_name, system);
            } else {
                right_mutable.insert(system_name, system);
            }
        }

        (
            Subset {
                immutable_systems: self.immutable_systems,
                mutable_systems: left_mutable,
            },
            Subset {
                immutable_systems: self.immutable_systems,
                mutable_systems: right_mutable,
            },
        )
    }

    /// Get a reference to an immutable system, or `None`, if it does not exist.
    pub fn immutable_system(&self, name: &'static str) -> Option<&System> {
        self.immutable_systems.get(name)
    }

    #[allow(clippy::mut_mut)]
    /// Get a mutable reference to a mutable system, or `None`, if it does not exist.
    pub fn mutable_system(&'a mut self, name: &'static str) -> Option<&'a mut &'a mut System<'a>> {
        self.mutable_systems.get_mut(name)
    }

    /// True if the immutable system does exist, false if not.
    pub fn has_immutable_system(&self, name: &'static str) -> bool {
        self.immutable_systems.contains_key(name)
    }

    /// True if the mutable system does exist, false if not.
    pub fn has_mutable_system(&self, name: &'static str) -> bool {
        self.mutable_systems.contains_key(name)
    }
}
