pub trait Sensor<Input, Output, Update> {
    fn read(&self, input: Input) -> Output;
    fn update(&mut self, update: Update);
}
