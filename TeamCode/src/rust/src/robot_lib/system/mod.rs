use std::collections::HashMap;

use crate::err::RobotError;
use crate::robot_lib::system::drivetrain::Drivetrain;
use std::fmt;
use std::fmt::{Display, Formatter};

pub mod drivetrain;

pub type Systems<'a> = HashMap<&'static str, System<'a>>;
pub type BorrowedSystems<'a> = &'a Systems<'a>;
pub type BorrowedMutSystems<'a> = HashMap<&'static str, &'a mut System<'a>>;

#[derive(Debug)]
pub enum System<'a> {
    Drivetrain(Drivetrain<'a>),
}

impl<'a> System<'a> {
    pub fn drivetrain(&mut self) -> Result<&mut Drivetrain<'a>, RobotError> {
        if let System::Drivetrain(drivetrain) = self {
            Ok(drivetrain)
        } else {
            Err(RobotError::SystemType(
                "Drivetrain".to_owned(),
                self.to_string(),
            ))
        }
    }
}

impl<'a> Display for System<'a> {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        match self {
            System::Drivetrain(_) => write!(f, "Drivetrain"),
        }
    }
}
