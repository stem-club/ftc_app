use crate::hardware::dcmotor::DcMotor;

#[derive(Debug)]
pub struct Drivetrain<'a> {
    left: DcMotor<'a>,
    right: DcMotor<'a>,
}

impl<'a> Drivetrain<'a> {
    pub fn new(left: DcMotor<'a>, right: DcMotor<'a>) -> Self {
        Drivetrain { left, right }
    }

    pub fn drive(&mut self, power: f64, turn: f64) {
        let mut left_power = power + turn;
        let mut right_power = power - turn;
        let max = left_power.abs().max(right_power.abs());
        if max > 1.0 {
            left_power /= max;
            right_power /= max;
        }
        self.tank_drive(left_power, right_power);
    }

    pub fn tank_drive(&mut self, left_power: f64, right_power: f64) {
        self.left.set_power(left_power);
        self.right.set_power(right_power);
    }
}
