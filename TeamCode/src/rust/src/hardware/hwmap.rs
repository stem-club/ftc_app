use std::collections::HashMap;

use jni::errors::ErrorKind;
use jni::objects::{JObject, JValue};
use jni::JNIEnv;
use log::error;

use crate::err::{RegisterHardwareError, RobotError};
use crate::hardware::Hardware;
use crate::util::str_to_jstring;

/// Used to access robot hardware by name. Created with `HWMapFactory`.
pub struct HWMap<'a> {
    map: HashMap<String, Hardware<'a>>,
}

impl<'a> HWMap<'a> {
    /// Remove a piece of hardware from the HWMap and return it, if it exists.
    ///
    /// # Parameters
    ///  - device_name: The name of the hardware. It should be the same as in the robot
    /// configuration and the name it was registered with in the factory.
    pub fn take(&mut self, device_name: &str) -> Result<Hardware<'a>, RobotError> {
        self.map
            .remove_entry(device_name)
            .map(|name_hardware| name_hardware.1)
            .ok_or_else(|| RobotError::UndefinedHardware(device_name.to_owned()))
    }
}

/// Factory used to create a new `HWMap`. Call `start`, end with `build`.
pub struct HWMapFactory<'a> {
    map: HashMap<String, Hardware<'a>>,
    hardware_map_contents: JObject<'a>,
    env: &'a JNIEnv<'a>,
}

impl<'a> HWMapFactory<'a> {
    /// Create a new HWMapFactory.
    ///
    /// # Parameters
    ///  - hardware_map_contents: Java `Map<String, HardwareDevice>` where exactly one name
    /// corresponds to one HardwareDevice and there is only one name per HardwareDevice. This helps
    /// ensure that Rust's guarantees are upheld.
    ///  - env: The JNI environment used to interface with Java
    pub fn start(hardware_map_contents: JObject<'a>, env: &'a JNIEnv<'a>) -> Self {
        HWMapFactory {
            map: HashMap::new(),
            hardware_map_contents,
            env,
        }
    }

    /// Register a new piece of hardware that is present in the map.
    ///
    /// # Parameters
    ///  - name: The name the hardware is registered under in the robot's configuration. This name
    /// will also be used to access the hardware from the HWMap when it is built.
    ///  - creator: A function that will be used to turn a Java object and JNI env into a Rust
    /// `Hardware`. Usually, this function will be present on the struct of the type of hardware
    /// that should be created (such as `DcMotorFactory::create_hardware`).
    pub fn register_hardware<F>(
        mut self,
        name: &'a str,
        creator: F,
    ) -> Result<Self, RegisterHardwareError>
    where
        F: Fn((JObject<'a>, &'a JNIEnv<'a>)) -> Hardware,
    {
        let j_name = JValue::Object(str_to_jstring(name, self.env).into());

        self.env
            .call_method(
                self.hardware_map_contents,
                "get",
                "(Ljava/lang/Object;)Ljava/lang/Object;",
                &[j_name],
            )
            .map(|hardware| {
                self.map
                    .insert(name.to_owned(), creator((hardware.l().unwrap(), &self.env)));
                self
            })
            .map_err(|error| match error.0 {
                ErrorKind::NullPtr(_) | ErrorKind::NullDeref(_) => {
                    RegisterHardwareError::DoesNotExist(name.to_string(), error)
                }
                ErrorKind::JavaException => RegisterHardwareError::UnknownException(error),
                _ => {
                    let message = format!("Error trying to register hardware: {}", error);
                    error!("{}", message);
                    panic!("{}", message)
                }
            })
    }

    /// Build a new HWMap from the factory.
    pub fn build(self) -> HWMap<'a> {
        HWMap { map: self.map }
    }
}
