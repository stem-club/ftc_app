use std::fmt;
use std::fmt::{Display, Formatter};

use crate::err::RobotError;
use crate::hardware::dcmotor::DcMotorFactory;

pub mod dcmotor;
pub mod hwmap;

#[derive(Debug)]
pub enum Hardware<'a> {
    DcMotor(DcMotorFactory<'a>),
}

impl<'a> Hardware<'a> {
    pub fn dc_motor(self) -> Result<DcMotorFactory<'a>, RobotError> {
        if let Hardware::DcMotor(dc_motor) = self {
            Ok(dc_motor)
        } else {
            Err(RobotError::HardwareType(
                "DcMotor".to_owned(),
                self.to_string(),
            ))
        }
    }
}

impl<'a> Display for Hardware<'a> {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        match self {
            Hardware::DcMotor(_) => write!(f, "DcMotor"),
        }
    }
}
