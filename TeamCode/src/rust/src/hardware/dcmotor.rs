use std::fmt;
use std::fmt::{Debug, Formatter};

use jni::objects::{JObject, JValue};
use jni::JNIEnv;

use crate::hardware::Hardware;

/// Analog to Java's `Direction`.
#[derive(Debug)]
pub enum Direction {
    Forward,
    Reverse,
}

/// Factory to create a `DcMotor`. End with `build`. `HWMap`s should contain these factories to allow
/// users to properly configure motors before use in an `OpMode`. They should only be created by
/// using `create_hardware` as `creator` when registering in `HWMap`s.
pub struct DcMotorFactory<'a> {
    motor: JObject<'a>,
    env: &'a JNIEnv<'a>,
    direction: Direction,
}

impl<'a> DcMotorFactory<'a> {
    /// Used as `creator` when registering in HWMaps.
    pub fn create_hardware(object_env: (JObject<'a>, &'a JNIEnv<'a>)) -> Hardware {
        Hardware::DcMotor(DcMotorFactory {
            motor: object_env.0,
            env: object_env.1,
            direction: Direction::Forward,
        })
    }

    /// Set the direction of a motor. This is the analog of Java's `DcMotor.setDirection`.
    ///
    /// # Parameters
    ///  - direction: The new direction of the motor
    pub fn direction(mut self, direction: Direction) -> Self {
        self.direction = direction;
        self
    }

    /// Build a new DcMotor from the factory.
    pub fn build(self) -> DcMotor<'a> {
        // TODO: Apply direction change

        DcMotor {
            motor: self.motor,
            env: self.env,
            power: 0.0,
        }
    }
}

impl<'a> Debug for DcMotorFactory<'a> {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "DcMotorFactory {{ direction: {:?} }}", self.direction)
    }
}

/// Rust representation of Java's `DcMotor`. See
/// [the FTC docs](https://ftctechnh.github.io/ftc_app/doc/javadoc/com/qualcomm/robotcore/hardware/DcMotor.html)
/// for more specific documentation on the methods. The Rust methods' names are the `snake_case`
/// version of the Java methods' names.
pub struct DcMotor<'a> {
    motor: JObject<'a>,
    env: &'a JNIEnv<'a>,
    power: f64,
}

impl<'a> DcMotor<'a> {
    // TODO: Implement all methods between here and the factory, see
    // https://ftctechnh.github.io/ftc_app/doc/javadoc/com/qualcomm/robotcore/hardware/DcMotor.html

    /// Set the motor power. Should be between -1 (full power reverse) and 1 (full power forward). A
    /// power of 0 means that the motor either brakes or coasts depending on the zero power
    /// behavior.
    pub fn set_power(&mut self, power: f64) -> &mut Self {
        self.env
            .call_method(self.motor, "setPower", "(D)V", &[JValue::Double(power)])
            .unwrap();
        self.power = power;
        self
    }

    /// Get the power the motor was last set at.
    pub fn get_power(&self) -> f64 {
        self.power
    }
}

impl<'a> Debug for DcMotor<'a> {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "DcMotor")
    }
}
