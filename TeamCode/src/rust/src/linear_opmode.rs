use jni::objects::{JObject, JValue};
use jni::JNIEnv;

use crate::hardware::hwmap::HWMap;

/// Rust wrapper around the Java `LinearOpMode` class. This struct doesn't run the `OpMode`. It just
/// supplies helper methods like `wait_for_start` and `sleep`.
pub struct LinearOpMode<'a> {
    env: &'a JNIEnv<'a>,
    this: JObject<'a>,
    hw_map: HWMap<'a>,
}

impl<'a> LinearOpMode<'a> {
    /// Create a new `LinearOpMode`
    ///
    /// # Parameters
    ///  - `env`: The JNI environment used to interface with Java
    ///  - `this`: The Java object representing the `LinearOpMode` (the `OpMode` that is accessed using
    /// `this` in Java)
    ///  - `hw_map`: The Rust `HWMap` containing all needed hardware
    pub fn new(env: &'a JNIEnv<'a>, this: JObject<'a>, hw_map: HWMap<'a>) -> Self {
        LinearOpMode { env, this, hw_map }
    }

    /// Wait until the "play" button is pushed on the Driver Station.
    pub fn wait_for_start(&self) {
        self.env
            .call_method(self.this, "waitForStart", "()V", &[])
            .unwrap();
    }

    /// Wait for some number of milliseconds.
    pub fn sleep(&self, ms: u32) {
        self.env
            .call_method(self.this, "sleep", "(J)V", &[JValue::Long(i64::from(ms))])
            .unwrap();
    }

    /// Get a mutable reference to the `LinearOpMode`'s `HWMap`
    pub fn hw_map(&mut self) -> &mut HWMap<'a> {
        &mut self.hw_map
    }
}
