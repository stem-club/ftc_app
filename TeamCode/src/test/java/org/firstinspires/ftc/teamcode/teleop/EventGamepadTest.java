package org.firstinspires.ftc.teamcode.teleop;

import com.qualcomm.robotcore.hardware.Gamepad;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class EventGamepadTest {
  Gamepad gamepad;
  EventGamepad eventGamepad;

  @Before
  public void before() {
    gamepad = new Gamepad();
    eventGamepad = new EventGamepad(gamepad);
  }

  @Test
  public void on_handleEvents() {
    EventListener listener = mock(EventListener.class);
    eventGamepad.on(ButtonState.PRESSED, Button.A, listener);

    eventGamepad.handleEvents();
    verify(listener, never()).onEvent();

    gamepad.a = true;

    eventGamepad.handleEvents();
    verify(listener, only()).onEvent();
  }

  @Test
  public void leftStick() {
    StickPosition stick = eventGamepad.leftStick();
    assertEquals(0, stick.x, 0.0001);
    assertEquals(0, stick.y, 0.0001);

    gamepad.left_stick_x = 0.5f;
    gamepad.left_stick_y = -0.5f;

    stick = eventGamepad.leftStick();

    assertEquals(0.5, stick.x, 0.0001);
    assertEquals(-0.5, stick.y, 0.0001);
  }

  @Test
  public void rightStick() {
    StickPosition stick = eventGamepad.rightStick();
    assertEquals(0, stick.x, 0.0001);
    assertEquals(0, stick.y, 0.0001);

    gamepad.right_stick_x = 0.5f;
    gamepad.right_stick_y = -0.5f;

    stick = eventGamepad.rightStick();

    assertEquals(0.5, stick.x, 0.0001);
    assertEquals(-0.5, stick.y, 0.0001);
  }

  @Test
  public void leftTrigger() {
    float trigger = eventGamepad.leftTrigger();
    assertEquals(0, trigger, 0.0001);

    gamepad.left_trigger = 0.5f;

    trigger = eventGamepad.leftTrigger();

    assertEquals(0.5, trigger, 0.0001);
  }

  @Test
  public void rightTrigger() {
    float trigger = eventGamepad.rightTrigger();
    assertEquals(0, trigger, 0.0001);

    gamepad.right_trigger = 0.5f;

    trigger = eventGamepad.rightTrigger();

    assertEquals(0.5, trigger, 0.0001);
  }
}