package org.firstinspires.ftc.teamcode.errors;

import org.firstinspires.ftc.teamcode.commands.Command;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class CommandUninitializedErrorTest {
    class ExampleCommand extends Command {
        @Override
        protected void onExecute() {}

        @Override
        protected void onFinish() {}

        @Override
        public boolean isFinished() {
            return true;
        }

        @Override
        protected Set<String> getRequiredSubsystems() {
            return new HashSet<>();
        }

        @Override
        protected Set<String> getIfAvailableSubsystems() {
            return new HashSet<>();
        }
    }

    @Test
    public void toString_() {
        assertEquals("Command ExampleCommand not initialized but accessed",
                new CommandUninitializedError(new ExampleCommand()).toString());
    }

    @Test
    public void toString_emptyString() {
        assertEquals("Command not initialized but accessed",
                new CommandUninitializedError(new Command() {
                    @Override
                    protected void onExecute() {}

                    @Override
                    protected void onFinish() {}

                    @Override
                    public boolean isFinished() {
                        return false;
                    }

                    @Override
                    protected Set<String> getRequiredSubsystems() {
                        return new HashSet<>();
                    }

                    @Override
                    protected Set<String> getIfAvailableSubsystems() {
                        return new HashSet<>();
                    }
                }).toString());
    }
}
