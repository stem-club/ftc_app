package org.firstinspires.ftc.teamcode.errors;

import org.junit.Test;

import static org.junit.Assert.*;

public class RequiredSubsystemUnavailableExceptionTest {
    @Test
    public void toString_() {
        assertEquals("Subsystem unavailable or does not exist: test",
                new RequiredSubsystemUnavailableException("test").toString());
    }
}