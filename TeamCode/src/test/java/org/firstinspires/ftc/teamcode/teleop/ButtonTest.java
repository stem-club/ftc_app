package org.firstinspires.ftc.teamcode.teleop;

import com.qualcomm.robotcore.hardware.Gamepad;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

public class ButtonTest {
  private Gamepad gamepad;

  @Before
  public void beforeEach() {
    gamepad = mock(Gamepad.class);
  }

  /**
   * This testing class depends on the fact that all buttons are false by default. If that ever changes, and only if
   * that changes, this should fail. If you're maintaining this codebase and this test fails, you need to figure out
   * what the new default is (true or random) and correct this class appropriately.
   */
  @Test
  public void falseByDefault() {
    assertFalse(gamepad.a);
  }

  @Test
  public void A() {
    gamepad.a = true;
    assertTrue(Button.A.extract(gamepad));
  }

  @Test
  public void B() {
    gamepad.b = true;
    assertTrue(Button.B.extract(gamepad));
  }

  @Test
  public void X() {
    gamepad.x = true;
    assertTrue(Button.X.extract(gamepad));
  }

  @Test
  public void Y() {
    gamepad.y = true;
    assertTrue(Button.Y.extract(gamepad));
  }

  @Test
  public void LEFT() {
    gamepad.dpad_left = true;
    assertTrue(Button.LEFT.extract(gamepad));
  }

  @Test
  public void RIGHT() {
    gamepad.dpad_right = true;
    assertTrue(Button.RIGHT.extract(gamepad));
  }

  @Test
  public void UP() {
    gamepad.dpad_up = true;
    assertTrue(Button.UP.extract(gamepad));
  }

  @Test
  public void DOWN() {
    gamepad.dpad_down = true;
    assertTrue(Button.DOWN.extract(gamepad));
  }

  @Test
  public void LEFT_BUMPER() {
    gamepad.left_bumper = true;
    assertTrue(Button.LEFT_BUMPER.extract(gamepad));
  }

  @Test
  public void RIGHT_BUMPER() {
    gamepad.right_bumper = true;
    assertTrue(Button.RIGHT_BUMPER.extract(gamepad));
  }

  @Test
  public void LEFT_STICK() {
    gamepad.left_stick_button = true;
    assertTrue(Button.LEFT_STICK.extract(gamepad));
  }

  @Test
  public void RIGHT_STICK() {
    gamepad.right_stick_button = true;
    assertTrue(Button.RIGHT_STICK.extract(gamepad));
  }
}