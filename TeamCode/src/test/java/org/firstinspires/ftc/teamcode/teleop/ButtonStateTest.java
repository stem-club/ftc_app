package org.firstinspires.ftc.teamcode.teleop;

import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * To any future maintainers: I know that this is a stupid unit test. It's pretty much useless. I only made it because I
 * wanted 100% coverage on all the classes.
 */
public class ButtonStateTest {
  private static List<ButtonState> buttonStates;

  @BeforeClass
  public static void beforeAll() {
    buttonStates = Arrays.asList(ButtonState.values());
  }

  @Test
  public void PRESSED() {
    assertTrue(buttonStates.contains(ButtonState.PRESSED));
  }

  @Test
  public void HELD() {
    assertTrue(buttonStates.contains(ButtonState.HELD));
  }

  @Test
  public void RELEASED() {
    assertTrue(buttonStates.contains(ButtonState.RELEASED));
  }

  @Test
  public void OFF() {
    assertTrue(buttonStates.contains(ButtonState.OFF));
  }
}