package org.firstinspires.ftc.teamcode.robot;

import org.firstinspires.ftc.teamcode.subsystems.ExampleImmutableSubsystem;
import org.firstinspires.ftc.teamcode.subsystems.ExampleMutableSubsystem;
import org.firstinspires.ftc.teamcode.subsystems.ImmutableSubsystem;
import org.firstinspires.ftc.teamcode.subsystems.MutableSubsystem;
import org.junit.Test;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class RobotTest {
    @Test
    public void subset() {
        Map<String, ImmutableSubsystem> immutable = new HashMap<>();
        immutable.put("immutableSystem", new ExampleImmutableSubsystem());

        Map<String, MutableSubsystem> mutable = new HashMap<>();
        mutable.put("mutableSystem1", new ExampleMutableSubsystem());
        mutable.put("mutableSystem2", new ExampleMutableSubsystem());

        Robot bot1 = new Robot(immutable, mutable);

        Set<String> mutableSubset = new HashSet<>();
        mutableSubset.add("mutableSystem2");
        Robot bot2 = bot1.subset(mutableSubset);

        assertNotNull(bot1.getMutableSubsystem("mutableSystem1"));
        assertNull(bot2.getMutableSubsystem("mutableSystem1"));

        assertNull(bot1.getMutableSubsystem("mutableSystem2"));
        assertNotNull(bot2.getMutableSubsystem("mutableSystem2"));

        assertNotNull(bot1.getImmutableSubsystem("immutableSystem"));
        assertNotNull(bot2.getImmutableSubsystem("immutableSystem"));
    }

    @Test
    public void union() {
        Map<String, ImmutableSubsystem> immutable = new HashMap<>();
        immutable.put("immutableSystem", new ExampleImmutableSubsystem());

        Map<String, MutableSubsystem> mutable = new HashMap<>();
        mutable.put("mutableSystem1", new ExampleMutableSubsystem());
        mutable.put("mutableSystem2", new ExampleMutableSubsystem());

        Robot bot1 = new Robot(immutable, mutable);

        Set<String> mutableSubset = new HashSet<>();
        mutableSubset.add("mutableSystem2");
        Robot bot2 = bot1.subset(mutableSubset);

        // Assume the subset is correct

        bot1.union(bot2);

        assertNotNull(bot1.getMutableSubsystem("mutableSystem1"));
        assertNotNull(bot1.getMutableSubsystem("mutableSystem2"));
        assertNotNull(bot1.getImmutableSubsystem("immutableSystem"));
    }

    @Test
    public void toString_() {
        Map<String, ImmutableSubsystem> immutable = new HashMap<>();
        Map<String, MutableSubsystem> mutable = new HashMap<>();

        Robot bot = new Robot(immutable, mutable);
        assertEquals("Immutable: " + immutable.toString() + ", Mutable: " + mutable.toString(), bot.toString());
    }
}