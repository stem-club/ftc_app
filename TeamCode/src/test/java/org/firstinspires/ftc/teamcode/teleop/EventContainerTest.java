package org.firstinspires.ftc.teamcode.teleop;

import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class EventContainerTest {
  private EventContainer eventContainer;

  @Before
  public void before() {
    eventContainer = new EventContainer();
  }

  @Test
  public void nextState_handle_addHandler() {
    EventListener pressed = mock(EventListener.class);
    eventContainer.addHandler(ButtonState.PRESSED, pressed);

    EventListener held = mock(EventListener.class);
    eventContainer.addHandler(ButtonState.HELD, held);

    EventListener released = mock(EventListener.class);
    eventContainer.addHandler(ButtonState.RELEASED, released);

    EventListener off = mock(EventListener.class);
    eventContainer.addHandler(ButtonState.OFF, off);

    // Start at OFF

    // Go to PRESSED
    eventContainer.nextState(true);
    eventContainer.handle();
    verify(pressed, times(1)).onEvent();

    // Go to HELD
    eventContainer.nextState(true);
    eventContainer.handle();
    verify(held, times(1)).onEvent();

    // Go to RELEASED
    eventContainer.nextState(false);
    eventContainer.handle();
    verify(released, times(1)).onEvent();

    // Go to OFF
    eventContainer.nextState(false);
    eventContainer.handle();
    verify(off, times(1)).onEvent();
  }
}