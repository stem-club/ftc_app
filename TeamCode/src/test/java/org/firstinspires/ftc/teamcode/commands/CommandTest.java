package org.firstinspires.ftc.teamcode.commands;

import org.firstinspires.ftc.teamcode.errors.CommandUninitializedError;
import org.firstinspires.ftc.teamcode.errors.RequiredSubsystemUnavailableException;
import org.firstinspires.ftc.teamcode.robot.Robot;
import org.firstinspires.ftc.teamcode.subsystems.ImmutableSubsystem;
import org.firstinspires.ftc.teamcode.subsystems.MutableSubsystem;
import org.junit.Test;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class CommandTest {
    @Test
    public void init_takesRequired() {
        Command command = new Command() {
            @Override
            protected void onExecute() {
                if (getMutableSubsystem("required") == null) {
                    fail("Correct subsystems do not exist");
                }
            }

            @Override
            protected void onFinish() {}

            @Override
            public boolean isFinished() {
                return true;
            }

            @Override
            protected Set<String> getRequiredSubsystems() {
                Set<String> required = new HashSet<>();
                required.add("required");
                return required;
            }

            @Override
            protected Set<String> getIfAvailableSubsystems() {
                return new HashSet<>();
            }
        };

        Map<String, ImmutableSubsystem> immutable = new HashMap<>();
        Map<String, MutableSubsystem> mutable = new HashMap<>();
        mutable.put("required", mock(MutableSubsystem.class));
        mutable.put("notRequired", mock(MutableSubsystem.class));

        Robot bot = new Robot(immutable, mutable);

        try {
            command.init(bot);
        } catch (RequiredSubsystemUnavailableException e) {
            fail("Required subsystem should be available");
        }

        assertNull(bot.getMutableSubsystem("required"));
        assertNotNull(bot.getMutableSubsystem("notRequired"));
    }

    @Test
    public void init_throwsErrorUnavailableRequired() {
        Command command = new Command() {
            @Override
            protected void onExecute() {}

            @Override
            protected void onFinish() {}

            @Override
            public boolean isFinished() {
                return true;
            }

            @Override
            protected Set<String> getRequiredSubsystems() {
                Set<String> required = new HashSet<>();
                required.add("doesNotExist");
                return required;
            }

            @Override
            protected Set<String> getIfAvailableSubsystems() {
                return new HashSet<>();
            }
        };

        Map<String, ImmutableSubsystem> immutable = new HashMap<>();
        Map<String, MutableSubsystem> mutable = new HashMap<>();
        mutable.put("subsystem", mock(MutableSubsystem.class));

        Robot bot = new Robot(immutable, mutable);

        try {
            command.init(bot);
            fail("Required subsystem is not available");
        } catch (RequiredSubsystemUnavailableException e) {
            // Should be thrown
        }

        assertNotNull(bot.getMutableSubsystem("subsystem"));
        assertNull(bot.getMutableSubsystem("doesNotExist"));
    }

    @Test
    public void init_takesIfAvailable() {
        Command command = new Command() {
            @Override
            protected void onExecute() {
                if (getMutableSubsystem("required") == null || getMutableSubsystem("ifAvailable") == null) {
                    fail("Correct subsystems do not exist");
                }
            }

            @Override
            protected void onFinish() {}

            @Override
            public boolean isFinished() {
                return true;
            }

            @Override
            protected Set<String> getRequiredSubsystems() {
                Set<String> required = new HashSet<>();
                required.add("required");
                return required;
            }

            @Override
            protected Set<String> getIfAvailableSubsystems() {
                Set<String> ifAvailable = new HashSet<>();
                ifAvailable.add("ifAvailable");
                return ifAvailable;
            }
        };

        Map<String, ImmutableSubsystem> immutable = new HashMap<>();
        Map<String, MutableSubsystem> mutable = new HashMap<>();
        mutable.put("required", mock(MutableSubsystem.class));
        mutable.put("ifAvailable", mock(MutableSubsystem.class));
        mutable.put("notRequested", mock(MutableSubsystem.class));

        Robot bot = new Robot(immutable, mutable);

        try {
            command.init(bot);
        } catch (RequiredSubsystemUnavailableException e) {
            fail("Required subsystem should be available");
        }

        assertNull(bot.getMutableSubsystem("required"));
        assertNull(bot.getMutableSubsystem("ifAvailable"));
        assertNotNull(bot.getMutableSubsystem("notRequested"));

        command.execute();
    }

    @Test
    public void init_noErrorUnavailableIfAvailable() {
        Command command = new Command() {
            @Override
            protected void onExecute() {}

            @Override
            protected void onFinish() {}

            @Override
            public boolean isFinished() {
                return true;
            }

            @Override
            protected Set<String> getRequiredSubsystems() {
                return new HashSet<>();
            }

            @Override
            protected Set<String> getIfAvailableSubsystems() {
                Set<String> ifAvailable = new HashSet<>();
                ifAvailable.add("doesNotExist");
                return ifAvailable;
            }
        };

        Map<String, ImmutableSubsystem> immutable = new HashMap<>();
        Map<String, MutableSubsystem> mutable = new HashMap<>();
        mutable.put("subsystem", mock(MutableSubsystem.class));

        Robot bot = new Robot(immutable, mutable);

        try {
            command.init(bot);
        } catch (RequiredSubsystemUnavailableException e) {
            fail("If available does not need to be available");
        }

        assertNotNull(bot.getMutableSubsystem("subsystem"));
        assertNull(bot.getMutableSubsystem("doesNotExist"));
    }

    @Test
    public void execute_callsOnExecute() {
        Command command = mock(Command.class);
        command.execute();
        verify(command).onExecute();
    }

    @Test
    public void getMutableSystem_errorBeforeInit() {
        Command command = new Command() {
            @Override
            protected void onExecute() {
                getMutableSubsystem("mutable");
            }

            @Override
            protected void onFinish() {}

            @Override
            public boolean isFinished() {
                return true;
            }

            @Override
            protected Set<String> getRequiredSubsystems() {
                Set<String> required = new HashSet<>();
                required.add("mutable");
                return required;
            }

            @Override
            protected Set<String> getIfAvailableSubsystems() {
                return new HashSet<>();
            }
        };

        try {
            command.execute();
            fail("Command was not initialized");
        } catch (CommandUninitializedError e) {
            // Expected
        }
    }

    @Test
    public void getImmutableSystem_errorBeforeInit() {
        Command command = new Command() {
            @Override
            protected void onExecute() {
                getImmutableSubsystem("immutable");
            }

            @Override
            protected void onFinish() {}

            @Override
            public boolean isFinished() {
                return true;
            }

            @Override
            protected Set<String> getRequiredSubsystems() {
                return new HashSet<>();
            }

            @Override
            protected Set<String> getIfAvailableSubsystems() {
                return new HashSet<>();
            }
        };

        try {
            command.execute();
            fail("Command was not initialized");
        } catch (CommandUninitializedError e) {
            // Expected
        }
    }

    @Test
    public void getImmutableSystem_getsImmutableSystem() {
        Command command = new Command() {
            @Override
            protected void onExecute() {
                getImmutableSubsystem("immutable");
            }

            @Override
            protected void onFinish() {}

            @Override
            public boolean isFinished() {
                return true;
            }

            @Override
            protected Set<String> getRequiredSubsystems() {
                return new HashSet<>();
            }

            @Override
            protected Set<String> getIfAvailableSubsystems() {
                return new HashSet<>();
            }
        };

        Map<String, ImmutableSubsystem> immutable = new HashMap<>();
        immutable.put("immutable", mock(ImmutableSubsystem.class));
        Map<String, MutableSubsystem> mutable = new HashMap<>();

        Robot bot = new Robot(immutable, mutable);

        try {
            command.init(bot);
        } catch (RequiredSubsystemUnavailableException e) {
            fail("Required subsystem should be available");
        }

        command.execute();
    }

    @Test
    public void finish_callsOnFinish() {
        Command command = mock(Command.class);
        command.finish();
        verify(command).onFinish();
    }
}
