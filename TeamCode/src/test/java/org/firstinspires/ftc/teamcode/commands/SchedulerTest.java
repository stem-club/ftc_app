package org.firstinspires.ftc.teamcode.commands;

import org.firstinspires.ftc.teamcode.robot.Robot;
import org.firstinspires.ftc.teamcode.subsystems.ImmutableSubsystem;
import org.firstinspires.ftc.teamcode.subsystems.MutableSubsystem;
import org.junit.Test;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class SchedulerTest {
    @Test
    public void scheduleCommand() {
        HashMap<String, ImmutableSubsystem> immutableSubsystems = new HashMap<>();
        HashMap<String, MutableSubsystem> mutableSubsystems = new HashMap<>();

        Robot bot = new Robot(immutableSubsystems, mutableSubsystems);
        Scheduler scheduler = new Scheduler(bot);

        Command command = mock(Command.class);

        scheduler.scheduleCommand(command);
        scheduler.run();

        verify(command).execute();
    }

    @Test
    public void scheduleCommand_falseWhenUnavailable() {
        HashMap<String, ImmutableSubsystem> immutableSubsystems = new HashMap<>();
        HashMap<String, MutableSubsystem> mutableSubsystems = new HashMap<>();

        Robot bot = new Robot(immutableSubsystems, mutableSubsystems);
        Scheduler scheduler = new Scheduler(bot);

        Command command = new Command() {
            @Override
            protected void onExecute() {}

            @Override
            protected void onFinish() {}

            @Override
            public boolean isFinished() {
                return false;
            }

            @Override
            protected Set<String> getRequiredSubsystems() {
                Set<String> requiredSubsystems = new HashSet<>();
                requiredSubsystems.add("doesNotExist");
                return requiredSubsystems;
            }

            @Override
            protected Set<String> getIfAvailableSubsystems() {
                return new HashSet<>();
            }
        };

        assertFalse(scheduler.scheduleCommand(command));
    }

    @Test
    public void run_triggersFinish() {
        HashMap<String, ImmutableSubsystem> immutableSubsystems = new HashMap<>();
        HashMap<String, MutableSubsystem> mutableSubsystems = new HashMap<>();

        Robot bot = new Robot(immutableSubsystems, mutableSubsystems);
        Scheduler scheduler = new Scheduler(bot);

        Command command = mock(Command.class);
        when(command.isFinished())
                .thenReturn(false)
                .thenReturn(true);

        scheduler.scheduleCommand(command);
        scheduler.run();
        verify(command).execute();

        scheduler.run();
        // Just ran the first time
        verify(command, times(1)).execute();
    }

    @Test
    public void run_triggersUnion() {
        Robot bot = mock(Robot.class);
        when(bot.subset(any()))
                .thenReturn(mock(Robot.class));
        Scheduler scheduler = new Scheduler(bot);

        Command command = new Command() {
            @Override
            protected void onExecute() {}

            @Override
            protected void onFinish() {}

            @Override
            public boolean isFinished() {
                return true;
            }

            @Override
            protected Set<String> getRequiredSubsystems() {
                return new HashSet<>();
            }

            @Override
            protected Set<String> getIfAvailableSubsystems() {
                return new HashSet<>();
            }
        };

        assertTrue(scheduler.scheduleCommand(command));
        scheduler.run();

        verify(bot).union(any());
    }
}