# The samples have been removed from this copy of the Git repository
View the most up to date samples online at
[the FTC app GitHub repository](https://github.com/ftctechnh/ftc_app/tree/master/FtcRobotController/src/main/java/org/firstinspires/ftc/robotcontroller/external/samples)